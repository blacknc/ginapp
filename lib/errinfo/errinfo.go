package errinfo

var (

	ErrOK = registerErrInfo("0", "succeed")

	// 错误码规则：模块（2位） + 错误大类(2位) + 错误子类(4类)
	// 模块：10

	// 系统相关(错误大类：01)
	ErrUnknown = registerErrInfo("10010001", "unknown error")
	ErrSystem = registerErrInfo("10010002", "system error")

	// mysql相关(错误大类：02)
	ErrDbConn = registerErrInfo("10020001", "conn db failed")
	ErrDbGetData = registerErrInfo("10020002", "db get data failed")

	// redis相关(错误大类：03)

	// rpc相关(错误大类：04)

	// 用户相关(错误大类：05)
	ErrParams = registerErrInfo("10050001", "params error")
)
