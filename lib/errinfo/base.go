package errinfo

import "fmt"

var errMap map[string]bool

type ErrInfo struct {
	errno string
	errmsg string
}

type RespErr struct {
	*ErrInfo
	bizerr string
}

func registerErrInfo(errno, errmsg string) *ErrInfo {

	if errMap == nil {
		errMap = make(map[string]bool)
	}
	if _, ok := errMap[errno]; ok {
		panic("errno existsed: " + errno)
	}
	return &ErrInfo{
		errno:  errno,
		errmsg: errmsg,
	}
}

func New(err error, ext ...interface{}) *RespErr  {

	var respErr *RespErr
	if err != nil {
		switch err.(type) {
		case *ErrInfo:
			respErr = &RespErr{
				ErrInfo: err.(*ErrInfo),
			}
		case *RespErr:
			respErr = err.(*RespErr)
		default:
			respErr = &RespErr{
				ErrInfo: ErrUnknown,
				bizerr:  err.Error(),
			}
		}
	} else {
		respErr = &RespErr{
			ErrInfo: ErrOK,
		}
	}

	if len(ext) > 0 {
		var bizerr string
		if format, ok := ext[0].(string); ok && format != "" && len(ext) > 1 {
			bizerr = fmt.Sprintf(format, ext[1:]...)
		} else {
			bizerr = fmt.Sprint(ext...)
		}
		respErr.bizerr = fmt.Sprintf("%s[%s]", bizerr, respErr.bizerr)
	}

	return respErr
}

func (e *ErrInfo)Error() string {
	return e.String()
}

func (e *ErrInfo)String() string {
	return fmt.Sprintf("errno[%s], errmsg[%s]", e.errno, e.errmsg)
}

func (e *ErrInfo)GetErrno() string {
	return e.errno
}

func (e *ErrInfo)GetErrmsg() string {
	return e.errmsg
}

func (e *RespErr)Error() string {
	return e.String()
}

func (e *RespErr)String() string {
	return fmt.Sprintf("errno[%s], errmsg[%s], bizerr[%s]", e.errno, e.errmsg, e.bizerr)
}

func (e *RespErr)GetBizerr() string {
	return e.bizerr
}

func (e *RespErr)SetBizerr(bizerr interface{}, ext ...interface{}) *RespErr {

	var bizerrStr string
	switch bizerr.(type) {
	case string:
		if len(ext) > 0 && bizerr.(string) != "" {
			bizerrStr = fmt.Sprintf(bizerr.(string), ext...)
		} else {
			bizerrStr = bizerr.(string)
		}
	default:
		if len(ext) > 0 {
			bizerrStr = fmt.Sprintf("%s %s", bizerr, fmt.Sprint(ext...))
		} else {
			bizerrStr = fmt.Sprint(bizerr)
		}
	}
	if e.bizerr != "" {
		e.bizerr = fmt.Sprintf("%s[%s]", bizerrStr, e.bizerr)
	} else {
		e.bizerr = bizerrStr
	}
	return e
}
