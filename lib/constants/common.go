package constants

const (

	LogDir = "./log"
	LogFileName = "service"

	// 请求响应数据KEY
	RespDataKey = "__resp_data"

	// Logid
	LogidKey = "__logid"

	// 请求开始时间
	ReqBeginTime = "__req_begin_time"

	ReqErrnoKey = "__req_errno"
	ReqErrmsgKey = "__req_errmsg"
	ReqBizerrKey = "__req_bizerr"
	ReqCostKey = "__req_cost"
)
