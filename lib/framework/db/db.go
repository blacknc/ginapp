// add by jiafangdong<jiafangdong@baidu.com> at 2020/8/2 4:04 下午
package db

import (
	"blacknc.top/ginapp/lib/framework/conf"
	"database/sql"
	"fmt"
	"github.com/didi/gendry/builder"
	"github.com/didi/gendry/manager"
	"github.com/didi/gendry/scanner"
	"log"
	"math/rand"
	"sync"
	"time"
)

func init() {
	scanner.SetTagName("json")
}

var (
	dbConnCache     sync.Map
	randomSingleton = rand.New(rand.NewSource(time.Now().Unix()))
)

type IDBConn interface {
	GetOne(table string, where map[string]interface{}, target interface{}) error
	GetMulti(table string, where map[string]interface{}, target interface{}) error
	Insert(table string, data []map[string]interface{}) (int64, error)
	Update(table string, where, data map[string]interface{}) (int64, error)
	Delete(table string, where map[string]interface{}) (int64, error)
}

type DBConn struct {
	*sql.DB
	DBConf *conf.DBConf
	err error
}

func GetConn(name string) IDBConn {

	dbConn, ok := dbConnCache.Load(name)
	if ok {
		return dbConn.(*DBConn)
	}

	return &DBConn{
		err:   fmt.Errorf("db `%s` is not inited", name),
	}
}

func InitWithFiles(confFiles ...string) {

	var err error
	for _, confFile := range confFiles {
		dbConf := &conf.DBConf{}
		err = conf.LoadConf(confFile, dbConf)
		if err != nil {
			log.Printf("[framework:db] load db conf file `%s` failed: %s", confFile, err)
			continue
		}
		dbConn, err := initDbConn(dbConf)
		if err != nil {
			log.Printf("[framework:db] init db conn `%s` failed: %s", dbConf.Name, err)
			continue
		}
		log.Printf("[framework:db] init db conn `%s` succeed", dbConf.Name)
		dbConnCache.Store(dbConf.Name, dbConn)
	}
}

func initDbConn(dbConf *conf.DBConf) (*DBConn, error) {

	var db *sql.DB
	var err error
	addr := selectDbAddr(dbConf.Hosts)
	db, err = manager.New(dbConf.DbName, dbConf.DbUser, dbConf.DbPasswd, addr.Host).
		Set(manager.SetCharset("utf8"),
			manager.SetAllowCleartextPasswords(true),
			manager.SetInterpolateParams(true),
			manager.SetTimeout(time.Duration(dbConf.Timeout)),
			manager.SetWriteTimeout(time.Duration(dbConf.WriteTimeout)),
			manager.SetReadTimeout(time.Duration(dbConf.ReadTimeout))).
		Port(addr.Port).
		Open(dbConf.Ping)
	if err != nil {
		return nil, fmt.Errorf("new db conn failed, addr[%v], err[%s]", addr, err)
	}
	dbConn := &DBConn{
		DB:     db,
		DBConf: dbConf,
	}
	return dbConn, nil
}

func selectDbAddr(addrs []*conf.Addr) *conf.Addr {
	if len(addrs) == 0 {
		panic("db addrs is empty")
	}
	return addrs[randomSingleton.Intn(len(addrs))]
}

//GetOne gets one matched records
func (dc *DBConn)GetOne(table string, where map[string]interface{}, target interface{}) error {

	if nil != dc.err {
		return fmt.Errorf("db conn is invalid, %s", dc.err)
	}
	cond, vals, err := builder.BuildSelect(table, where, nil)
	if nil != err {
		return err
	}
	row, err := dc.Query(cond, vals...)
	if nil != err || nil == row {
		return err
	}
	defer row.Close()
	err = scanner.Scan(row, target)
	return nil
}

//GetMulti gets multiple matched records
func (dc *DBConn)GetMulti(table string, where map[string]interface{}, target interface{}) error {

	if nil != dc.err {
		return fmt.Errorf("db conn is invalid, %s", dc.err)
	}
	cond, vals, err := builder.BuildSelect(table, where, nil)
	if nil != err {
		return err
	}
	row, err := dc.Query(cond, vals...)
	if nil != err || nil == row {
		return err
	}
	defer row.Close()
	err = scanner.Scan(row, target)
	return err
}

//Insert inserts an array of data
func (dc *DBConn)Insert(table string, data []map[string]interface{}) (int64, error) {

	if nil != dc.err {
		return 0, fmt.Errorf("db conn is invalid, %s", dc.err)
	}
	cond, vals, err := builder.BuildInsert(table, data)
	if nil != err {
		return 0, err
	}
	result, err := dc.Exec(cond, vals...)
	if nil != err || nil == result {
		return 0, err
	}
	return result.LastInsertId()
}

// Update updates matched records
func (dc *DBConn)Update(table string, where, data map[string]interface{}) (int64, error) {

	if nil != dc.err {
		return 0, fmt.Errorf("db conn is invalid, %s", dc.err)
	}
	cond, vals, err := builder.BuildUpdate(table, where, data)
	if nil != err {
		return 0, err
	}
	result, err := dc.Exec(cond, vals...)
	if nil != err {
		return 0, err
	}
	return result.RowsAffected()
}

// Delete deletes matched records
func (dc *DBConn)Delete(table string, where map[string]interface{}) (int64, error) {

	if nil != dc.err {
		return 0, fmt.Errorf("db conn is invalid, %s", dc.err)
	}
	cond, vals, err := builder.BuildDelete(table, where)
	if nil != err {
		return 0, err
	}
	result, err := dc.Exec(cond, vals...)
	if nil != err {
		return 0, err
	}
	return result.RowsAffected()
}