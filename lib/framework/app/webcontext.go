package app

import (
	"blacknc.top/ginapp/lib/framework/logger"
	"github.com/gin-gonic/gin"
)

type WebContext struct {
	*gin.Context
	*logger.Logger
}
