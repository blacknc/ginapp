// add by jiafangdong<jiafangdong@baidu.com> at 2020/8/1 9:33 下午
package app

import (
	"blacknc.top/ginapp/lib/framework/logger"
	"github.com/gin-gonic/gin"
	"log"
)

type WebServer struct {
	*WebRouter
	*gin.Engine
}

type WebHandlerFunc func(ctx *WebContext)

func NewWebServer(runMode string) *WebServer {
	gin.SetMode(runMode)
	webServer := &WebServer{
		Engine: gin.Default(),
		WebRouter: &WebRouter{
			root:        true,
		},
	}
	webServer.WebRouter.RouterGroup = &webServer.Engine.RouterGroup
	webServer.WebRouter.server = webServer
	return webServer
}

func (w *WebServer)Run(addr string) {
	err := w.Engine.Run(addr)
	if err != nil {
		log.Fatalf("[framework] webserver run failed, err[%s]", err)
	}
}

func (w *WebServer)Use(middlewares ...WebHandlerFunc) IWebRoutes {
	return w.WebRouter.Use(middlewares...)
}

func convertWebHandlerToGinHandler(handlers ...WebHandlerFunc) []gin.HandlerFunc {
	var ginHandlers []gin.HandlerFunc
	for _, handler := range handlers {
		ginHandler := func(ctx *gin.Context) {
			webCtx := &WebContext{
				Context: ctx,
				Logger:  logger.GetDefaultLogger(),
			}
			handler(webCtx)
		}
		ginHandlers = append(ginHandlers, ginHandler)
	}
	return ginHandlers
}
