package app

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type IWebRouteGroup interface {
	IWebRoutes
	Group(string, ...WebHandlerFunc) IWebRouteGroup
}

// IWebRoutes defines all router handle interface.
type IWebRoutes interface {
	Use(...WebHandlerFunc) IWebRoutes

	Handle(string, string, ...WebHandlerFunc) IWebRoutes
	Any(string, ...WebHandlerFunc) IWebRoutes
	GET(string, ...WebHandlerFunc) IWebRoutes
	POST(string, ...WebHandlerFunc) IWebRoutes
	DELETE(string, ...WebHandlerFunc) IWebRoutes
	PATCH(string, ...WebHandlerFunc) IWebRoutes
	PUT(string, ...WebHandlerFunc) IWebRoutes
	OPTIONS(string, ...WebHandlerFunc) IWebRoutes
	HEAD(string, ...WebHandlerFunc) IWebRoutes

	StaticFile(string, string) IWebRoutes
	Static(string, string) IWebRoutes
	StaticFS(string, http.FileSystem) IWebRoutes
}

type WebRouter struct {
	*gin.RouterGroup
	server *WebServer
	root bool
}

func (r *WebRouter)Group(relativePath string, handlers ...WebHandlerFunc) IWebRouteGroup {
	return &WebRouter{
		RouterGroup: r.RouterGroup.Group(relativePath, convertWebHandlerToGinHandler(handlers...)...),
		server:      r.server,
		root:        false,
	}
}

func (r *WebRouter) Use(middleware ...WebHandlerFunc) IWebRoutes {
	r.RouterGroup.Use(convertWebHandlerToGinHandler(middleware...)...)
	return r.returnObj()
}

func (r *WebRouter) Handle(httpMethod, relativePath string, handlers ...WebHandlerFunc) IWebRoutes {
	r.RouterGroup.Handle(httpMethod, relativePath, convertWebHandlerToGinHandler(handlers...)...)
	return r.returnObj()
}

func (r *WebRouter) Any(relativePath string, handlers ...WebHandlerFunc) IWebRoutes {
	r.RouterGroup.Any(relativePath, convertWebHandlerToGinHandler(handlers...)...)
	return r.returnObj()
}

func (r *WebRouter) GET(relativePath string, handlers ...WebHandlerFunc) IWebRoutes {
	r.RouterGroup.GET(relativePath, convertWebHandlerToGinHandler(handlers...)...)
	return r.returnObj()
}

func (r *WebRouter) POST(relativePath string, handlers ...WebHandlerFunc) IWebRoutes {
	r.RouterGroup.POST(relativePath, convertWebHandlerToGinHandler(handlers...)...)
	return r.returnObj()
}

func (r *WebRouter) DELETE(relativePath string, handlers ...WebHandlerFunc) IWebRoutes {
	r.RouterGroup.DELETE(relativePath, convertWebHandlerToGinHandler(handlers...)...)
	return r.returnObj()
}

func (r *WebRouter) PATCH(relativePath string, handlers ...WebHandlerFunc) IWebRoutes {
	r.RouterGroup.PATCH(relativePath, convertWebHandlerToGinHandler(handlers...)...)
	return r.returnObj()
}

func (r *WebRouter) PUT(relativePath string, handlers ...WebHandlerFunc) IWebRoutes {
	r.RouterGroup.PUT(relativePath, convertWebHandlerToGinHandler(handlers...)...)
	return r.returnObj()
}

func (r *WebRouter) OPTIONS(relativePath string, handlers ...WebHandlerFunc) IWebRoutes {
	r.RouterGroup.OPTIONS(relativePath, convertWebHandlerToGinHandler(handlers...)...)
	return r.returnObj()
}

func (r *WebRouter) HEAD(relativePath string, handlers ...WebHandlerFunc) IWebRoutes {
	r.RouterGroup.HEAD(relativePath, convertWebHandlerToGinHandler(handlers...)...)
	return r.returnObj()
}

func (r *WebRouter) StaticFile(relativePath, filePath string) IWebRoutes {
	r.RouterGroup.StaticFile(relativePath, filePath)
	return r.returnObj()
}

func (r *WebRouter) Static(relativePath, root string) IWebRoutes {
	r.RouterGroup.Static(relativePath, root)
	return r.returnObj()
}

func (r *WebRouter) StaticFS(relativePath string, fs http.FileSystem) IWebRoutes {
	r.RouterGroup.StaticFS(relativePath, fs)
	return r.returnObj()
}


func (r *WebRouter) returnObj() IWebRoutes {
	if r.root {
		return r.server
	}
	return r
}

