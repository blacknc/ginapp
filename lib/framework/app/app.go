// add by jiafangdong<jiafangdong@baidu.com> at 2020/7/31 7:30 下午
package app

import (
	"blacknc.top/ginapp/lib/framework/conf"
	"blacknc.top/ginapp/lib/framework/db"
	"blacknc.top/ginapp/lib/framework/logger"
	"blacknc.top/ginapp/lib/framework/redis"
	"log"
)

var (
	defaultAppConfFileName = "app.toml"
	defaultLogConfFileName = "log.toml"
	defaultDBConfDir       = "db"
	defaultRedisConfDir    = "redis"
	defaultAppName         = "ginapp"
	defaultListen          = "localhost:8089"
	defaultRunMode         = "debug"
)

type App struct {
	appConf   *conf.AppConf
	webServer *WebServer
}

func NewApp() *App {

	return &App{}
}

func (a *App) Init() {

	// 初始化app配置
	appConf := &conf.AppConf{}
	if err := conf.LoadConf(defaultAppConfFileName, appConf); err != nil {
		log.Fatalf("[framework:app] load app config failed: %s", err)
	}
	a.appConf = appConf

	// 初始化app各项功能配置（日志、数据库、redis）
	a.initWithConf()
}

func (a *App) initWithConf() {

	if a.appConf.AppName == "" {
		a.appConf.AppName = defaultAppName
	}
	if a.appConf.Listen == "" {
		a.appConf.Listen = defaultListen
	}
	if a.appConf.RunMode == "" {
		a.appConf.RunMode = defaultRunMode
	}

	a.webServer = NewWebServer(a.appConf.RunMode)

	a.initLogger()
	a.initRedis()
	a.initDb()

	log.Println("[framework:app] app init succeed")
}

func (a *App) initLogger() {

	logConf := &conf.LogConf{}
	if err := conf.LoadConf(defaultLogConfFileName, logConf); err != nil {
		log.Fatalf("[framework:app] load log conf failed: %s", err)
	}

	logger.SetLogConf(logConf)
}

func (a *App) initRedis() {
	confFileList := conf.ListConfFile(defaultRedisConfDir)
	redis.InitWithFiles(confFileList...)
}

func (a *App) initDb() {
	confFileList := conf.ListConfFile(defaultDBConfDir)
	db.InitWithFiles(confFileList...)
}

type RouterRegister func(w *WebServer)

func (a *App) RegisterRouter(register RouterRegister) {
	register(a.webServer)
}

func (app *App) Run() {
	app.webServer.Run(app.appConf.Listen)
}
