// add by jiafangdong<jiafangdong@baidu.com> at 2020/7/31 7:39 下午
package env

import (
	"log"
	"os"
	"path/filepath"
)

var (
	rootPath = ""
	confPath = ""
)

func init()  {
	detectRootPath()
}

func detectRootPath()  {

	var curDir string
	var err error

	curDir, err = os.Getwd()
	if err != nil {
		panic("Detected current directory failed: " + err.Error())
	}

	// 检查当前目录是否有配置目录conf
	confDir := filepath.Join(curDir, "conf")
	if _, err := os.Stat(confDir); os.IsNotExist(err) {
		panic("Current directory not found conf dir, current directory: " + curDir)
	}

	rootPath = curDir
	confPath = confDir

	log.Println("[framework:env] RootPath:", rootPath)
	log.Println("[framework:env] ConfPath: ", confPath)
}

func GetRootPath() string {
	return rootPath
}

func GetConfPath() string  {
	return confPath
}
