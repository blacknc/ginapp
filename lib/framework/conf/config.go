// add by jiafangdong<jiafangdong@baidu.com> at 2020/7/31 7:30 下午
package conf

import (
	"blacknc.top/ginapp/lib/framework/env"
	"fmt"
	"github.com/BurntSushi/toml"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"sync"
)

var (
	confCache sync.Map
)

type AppConf struct {
	AppName string `toml:"app_name"`
	RunMode string `toml:"run_mode"`
	Listen  string `toml:"listen"`
}

type LogConf struct {
	Name         string `toml:"name"`
	Path         string `toml:"path"`
	Level        string `toml:"level"`
	Suffix       string `toml:"suffix"`
	Maxage       int    `toml:"maxage"`
	RotationTime int    `toml:"rotation_time"`
}

type DBConf struct {
	Name         string  `toml:"name"`
	Driver       string  `toml:"driver"`
	Timeout      int     `toml:"timeout"`
	ReadTimeout  int     `toml:"read_timeout"`
	WriteTimeout int     `toml:"write_timeout"`
	DbName       string  `toml:"db_name"`
	DbUser       string  `toml:"db_user"`
	DbPasswd     string  `toml:"db_passwd"`
	Hosts        []*Addr `toml:"hosts"`
	Ping         bool    `toml:"ping"`
}

type RedisConf struct {
	Name            string  `toml:"name"`
	Timeout         int     `toml:"timeout"`
	ReadTimeout     int     `toml:"read_timeout"`
	WriteTimeout    int     `toml:"write_timeout"`
	MaxConnLifeTime int     `toml:"max_conn_life_time"`
	MaxIdleConn     int     `toml:"max_idle_conn"`
	MaxActiveConn   int     `toml:"max_active_conn"`
	IdleTimeout     int     `toml:"idle_timeout"`
	Hosts           []*Addr `toml:"hosts"`
	Ping            bool    `toml:"ping"`
}

type Addr struct {
	Host string `toml:"host"`
	Port int    `toml:"port"`
}

func (a *Addr)String() string {
	return fmt.Sprintf("%s:%d", a.Host, a.Port)
}

func LoadConf(confFile string, conf interface{}) (err error) {

	confFilePath := filepath.Join(env.GetConfPath(), confFile)

	confData, ok := confCache.Load(confFilePath)
	if ok {
		reflect.ValueOf(conf).Elem().Set(reflect.ValueOf(confData))
		return
	}

	if _, err = os.Stat(confFilePath); err != nil {
		return
	}

	if _, err = toml.DecodeFile(confFilePath, conf); err != nil {
		return
	}

	confCache.Store(confFilePath, reflect.ValueOf(conf).Elem().Interface())

	return
}

func ListConfFile(dirname string) []string {

	pattern := filepath.Join(env.GetConfPath(), dirname, "*.toml")
	fileList, err := filepath.Glob(pattern)
	if err != nil {
		panic("[framework:conf] list conf file failed: " + err.Error())
	}

	var confFileList []string
	for _, f := range fileList {
		name, _ := filepath.Rel(env.GetConfPath(), f)
		if name == "" || strings.HasPrefix(filepath.Base(name), ".") {
			continue
		}
		confFileList = append(confFileList, name)
	}
	return confFileList
}
