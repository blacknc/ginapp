// add by jiafangdong<jiafangdong@baidu.com> at 2020/8/2 3:41 下午
package logger

import (
	"blacknc.top/ginapp/lib/framework/conf"
	"fmt"
	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
	"github.com/rifflock/lfshook"
	"github.com/sirupsen/logrus"
	"log"
	"os"
	"path"
	"sync"
	"time"
)

type Logger struct {
	*logrus.Logger
}

var (
	defaultLogConf = &conf.LogConf{
		Name:         "service",
		Path:         "log",
		Level:        "debug",
		Suffix:       "%Y%m%d.log",
		Maxage:       7,
		RotationTime: 24,
	}

	defaultLogDir = "log"
	defaultLogName = "service"

	loggerCache sync.Map
	mu sync.Mutex

	logLevelMap = map[string]logrus.Level{
		"panic": logrus.PanicLevel,
		"fatal": logrus.FatalLevel,
		"error": logrus.ErrorLevel,
		"warn":  logrus.WarnLevel,
		"info":  logrus.InfoLevel,
		"debug": logrus.DebugLevel,
		"trace": logrus.TraceLevel,
	}
)

func GetDefaultLogger() *Logger {
	return GetLogger(defaultLogName)
}

func GetLogger(name string) *Logger {

	if name == "" {
		name = defaultLogName
	}
	logins, ok := loggerCache.Load(name)
	if ok {
		return logins.(*Logger)
	}

	mu.Lock()
	defer mu.Unlock()

	logins, ok = loggerCache.Load(name)
	if ok {
		return logins.(*Logger)
	}

	newLogins := newLogger(name)
	loggerCache.Store(name, newLogins)

	return newLogins
}

func SetLogConf(logConf *conf.LogConf)  {
	defaultLogConf = logConf
}

func newLogger(name string) *Logger {

	var logins = &Logger{
		logrus.New(),
	}
	logDir, logFileName := defaultLogDir, defaultLogName

	if defaultLogConf.Path != "" {
		logDir = defaultLogConf.Path
	}
	if name != "" {
		logFileName = name
	} else if defaultLogConf.Name != "" {
		logFileName = defaultLogConf.Name
	}

	// 日志文件
	fileName := path.Join(logDir, logFileName)

	// 生成日志目录，清理历史文件
	var err error
	_, err = os.Stat(logDir)
	if os.IsNotExist(err) {
		err = os.Mkdir(logDir, 0700)
		if err != nil {
			log.Fatalf("[framework:logger] make log dir failed: %s", err)
		}
	}
	err = os.Remove(fileName)
	if err != nil && !os.IsNotExist(err) {
		log.Fatalf("[framework:logger] clean log file failed: %s", err)
	}

	// 打开文件
	src, err := os.OpenFile(fileName, os.O_CREATE|os.O_WRONLY|os.O_APPEND, os.ModeAppend)
	if err != nil {
		log.Fatalf("[framework:logger] open log file failed: %s", err)
	}

	// 设置输出
	logins.Out = src

	// 设置日志级别
	logins.SetLevel(convertLogLevel(defaultLogConf.Level))

	// 设置 rotatelogs
	logWriter, err := rotatelogs.New(
		// 分割后的文件名称
		fmt.Sprintf("%s.%s", fileName, defaultLogConf.Suffix),

		// 生成软链，指向最新日志文件
		rotatelogs.WithLinkName(fileName),

		// 设置最大保存时间
		rotatelogs.WithMaxAge(time.Duration(defaultLogConf.Maxage)*24*time.Hour),

		// 设置日志切割时间间隔
		rotatelogs.WithRotationTime(time.Duration(defaultLogConf.RotationTime)*time.Hour),
	)

	writeMap := lfshook.WriterMap{
		logrus.InfoLevel:  logWriter,
		logrus.FatalLevel: logWriter,
		logrus.DebugLevel: logWriter,
		logrus.WarnLevel:  logWriter,
		logrus.ErrorLevel: logWriter,
		logrus.PanicLevel: logWriter,
	}

	lfHook := lfshook.NewHook(writeMap, &logrus.TextFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
	})

	// 新增 Hook
	logins.AddHook(lfHook)

	return logins
}

func convertLogLevel(level string) logrus.Level {
	return logLevelMap[level]
}
