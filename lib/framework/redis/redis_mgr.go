// add by jiafangdong<jiafangdong@baidu.com> at 2020/8/2 4:04 下午
package redis

import (
	"blacknc.top/ginapp/lib/framework/conf"
	"fmt"
	"github.com/gomodule/redigo/redis"
	"log"
	"math/rand"
	"sync"
	"time"
)

var (
	redisPoolCache sync.Map
	// 空闲连接最大数量
	defaultMaxIdeConn = 1000
	// 活跃连接最大数量
	defaultMaxActiveConn = 500
	// 连接最大存活时间
	defaultMaxConnLifeTime = 60 * time.Second
	// 连接最大空闲时间
	defaultIdleTimeout = 60 * time.Second
	// 默认连接超时时间
	defaultConnTimeout = 1500 * time.Millisecond
	// 默认读数据超时时间
	defaultReadTimeout = 1500 * time.Millisecond
	// 默认写数据超时时间
	defaultWriteTimeout = 1500 * time.Millisecond

	randomSingleton = rand.New(rand.NewSource(time.Now().Unix()))

	// 类型转换方法映射
	Bool       = redis.Bool
	ByteSlices = redis.ByteSlices
	Bytes      = redis.Bytes
	Float64    = redis.Float64
	Float64s   = redis.Float64s
	Int        = redis.Int
	Int64      = redis.Int64
	Int64s     = redis.Int64s
	IntMap     = redis.IntMap
	Ints       = redis.Ints
	Scan       = redis.Scan
	ScanSlice  = redis.ScanSlice
	ScanStruct = redis.ScanStruct
	String     = redis.String
	StringMap  = redis.StringMap
	Strings    = redis.Strings
	Uint64     = redis.Uint64
	Values     = redis.Values
)

func getConn(name string) (redis.Conn, error) {

	pool, ok := redisPoolCache.Load(name)
	if ok {
		conn := pool.(*redis.Pool).Get()
		return conn, nil
	}

	return nil, fmt.Errorf("redis pool not found `%s`", name)
}

func InitWithFiles(confFiles ...string) {

	var err error
	for _, confFile := range confFiles {
		redisConf := &conf.RedisConf{}
		err = conf.LoadConf(confFile, redisConf)
		if err != nil {
			log.Printf("[framework:redis] load redis conf file `%s` failed: %s", confFile, err)
			continue
		}
		redisPool := initRedisPool(redisConf)
		log.Printf("[framework:redis] init redis pool `%s` succeed", redisConf.Name)
		redisPoolCache.Store(redisConf.Name, redisPool)
	}
}

func initRedisPool(redisConf *conf.RedisConf) (redisPool *redis.Pool) {

	addr := selectRedisAddr(redisConf.Hosts)
	redisPool = &redis.Pool{
		MaxIdle:         parseMaxIdleConn(redisConf.MaxIdleConn),
		MaxActive:       parseMaxActiveConn(redisConf.MaxActiveConn),
		IdleTimeout:     parseIdleTimeout(redisConf.IdleTimeout),
		MaxConnLifetime: parseMaxConnLiftTime(redisConf.MaxConnLifeTime),
		Wait:            true,
		TestOnBorrow: func(conn redis.Conn, t time.Time) error {
			_, err := conn.Do("PING")
			return err
		},
		Dial: func() (conn redis.Conn, e error) {
			return redis.Dial(
				"tcp",
				addr.String(),
				redis.DialConnectTimeout(parseTimeout(redisConf.Timeout)),
				redis.DialReadTimeout(parseReadTimeout(redisConf.ReadTimeout)),
				redis.DialWriteTimeout(parseWriteTimeout(redisConf.WriteTimeout)),
			)
		},
	}
	return
}

func selectRedisAddr(addrs []*conf.Addr) *conf.Addr {
	if len(addrs) == 0 {
		panic("redis addrs is empty")
	}
	return addrs[randomSingleton.Intn(len(addrs))]
}

func parseIdleTimeout(idleTimeoutSec int) time.Duration {
	if idleTimeoutSec < 0 {
		return defaultIdleTimeout
	}
	return time.Duration(idleTimeoutSec) * time.Second
}

func parseMaxIdleConn(maxIdleConn int) int {
	if maxIdleConn < 0 || maxIdleConn > 9999 {
		return defaultMaxIdeConn
	}
	return maxIdleConn
}

func parseMaxActiveConn(maxActiveConn int) int {
	if maxActiveConn < 0 || maxActiveConn > 9999 {
		return defaultMaxActiveConn
	}
	return maxActiveConn
}

func parseMaxConnLiftTime(maxConnLiftTimeSec int) time.Duration {
	if maxConnLiftTimeSec < 0 {
		return defaultMaxConnLifeTime
	}
	return time.Duration(maxConnLiftTimeSec) * time.Second
}

func parseTimeout(timeout int) time.Duration {
	if timeout < 0 {
		return defaultConnTimeout
	}
	return time.Duration(timeout) * time.Millisecond
}

func parseReadTimeout(timeout int) time.Duration {
	if timeout < 0 {
		return defaultReadTimeout
	}
	return time.Duration(timeout) * time.Millisecond
}

func parseWriteTimeout(timeout int) time.Duration {
	if timeout < 0 {
		return defaultWriteTimeout
	}
	return time.Duration(timeout) * time.Millisecond
}
