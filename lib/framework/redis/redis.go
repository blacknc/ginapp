// add by jiafangdong<jiafangdong@baidu.com> at 2020/8/2 4:04 下午
package redis

import (
	"blacknc.top/ginapp/lib/framework/logger"
	"fmt"
)

type IRedisConn interface {
	Do(cmd string, args ...interface{}) (reply interface{}, err error)
}

type RedisConn struct {
	ServiceName string
}

func GetConn(name string) IRedisConn {
	return &RedisConn{ServiceName:name}
}

func (r *RedisConn)Do(cmd string, args ...interface{}) (reply interface{}, err error) {

	rc, err := getConn(r.ServiceName)
	if err != nil {
		logger.GetLogger("redis").Warnf("get redis conn failed, err[%s]", err)
		return nil, fmt.Errorf("get redis conn failed, err[%s]", err)
	}
	defer rc.Close()

	reply, err = rc.Do(cmd, args...)
	if err != nil {
		logger.GetLogger("redis").Warnf("exec redis cmd failed, cmd[%s], err[%s]", cmd, err)
	}
	return reply, err
}