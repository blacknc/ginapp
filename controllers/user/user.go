package user

import (
	"blacknc.top/ginapp/lib/errinfo"
	"blacknc.top/ginapp/lib/framework/app"
	"blacknc.top/ginapp/lib/framework/db"
	"blacknc.top/ginapp/lib/framework/redis"
	daodb "blacknc.top/ginapp/models/dao/db"
	"blacknc.top/ginapp/models/dao/db/b3_solo_user"
	"encoding/json"
	"log"
)

func (c *controller) Register(ctx *app.WebContext) {

	var err error
	dc := db.GetConn("solo_read")

	type UserInfo struct {
		OId          string `form:"oId"`
		UserName     string `form:"userName"`
		UserURL      string `form:"userURL"`
		UserRole     string `form:"userRole"`
		UserAvatar   string `form:"userAvatar"`
		UserB3Key    string `form:"userB3Key"`
		UserGitHubID string `form:"userGitHubId"`
	}
	var soloUser UserInfo
	if err = c.ShouldBind(ctx, &soloUser); err != nil {
		c.Fail(ctx, err)
		return
	}
	data := daodb.Data{
		"oId": soloUser.OId,
		"userName": soloUser.UserName,
		"userURL": soloUser.UserURL,
		"userRole": soloUser.UserRole,
		"userAvatar": soloUser.UserAvatar,
		"userB3key": soloUser.UserB3Key,
		"userGitHubID": soloUser.UserGitHubID,
	}
	oID, err := b3_solo_user.InsertOne(dc, data)
	if err != nil {
		c.Fail(ctx, errinfo.New(errinfo.ErrDbGetData).SetBizerr(err))
		return
	}
	c.Succ(ctx, oID)
}

func (c *controller) Get(ctx *app.WebContext) {

	var err error
	var soloUser *b3_solo_user.B3SoloUser

	rc := redis.GetConn("solo")
	reply, err := redis.String(rc.Do("GET", "user"))
	if err == nil || reply != "" {
		soloUser = &b3_solo_user.B3SoloUser{}
		if err = json.Unmarshal([]byte(reply), &soloUser); err == nil {
			c.Succ(ctx, soloUser)
			return
		}
	}

	dc := db.GetConn("solo_read")
	where := daodb.Where{
		"_limit": []uint{0, 1},
	}
	soloUser, err = b3_solo_user.GetOne(dc, where)
	if err != nil {
		c.Fail(ctx, errinfo.New(errinfo.ErrDbGetData).SetBizerr(err))
		return
	}
	go func() {
		soloUserStr, err := json.Marshal(*soloUser)
		if err != nil {
			log.Println(err)
			return
		}
		rc.Do("SET", "user", string(soloUserStr), "EX", 60)
	}()

	c.Succ(ctx, soloUser)
}
