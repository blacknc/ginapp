package controllers

import (
	"blacknc.top/ginapp/lib/constants"
	"blacknc.top/ginapp/lib/errinfo"
	"blacknc.top/ginapp/lib/framework/app"
)

type BaseController struct {
}

func (b *BaseController)Succ(ctx *app.WebContext, data interface{}) {
	ctx.Set(constants.RespDataKey, data)
}

func (b *BaseController)Fail(ctx *app.WebContext, err error) {
	ctx.Set(constants.RespDataKey, err)
}

func (b *BaseController)ShouldBind(ctx *app.WebContext, target interface{}) error {
	if err := ctx.ShouldBind(target); err != nil {
		return errinfo.New(errinfo.ErrParams).SetBizerr(err)
	}
	return nil
}