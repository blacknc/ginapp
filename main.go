package main

import (
	"blacknc.top/ginapp/lib/framework/app"
	"blacknc.top/ginapp/routers"
	_ "github.com/go-sql-driver/mysql"
)

func main()  {

	appins := app.NewApp()
	appins.Init()
	appins.RegisterRouter(routers.Init)
	appins.Run()
}
