package middlewares

import (
	"blacknc.top/ginapp/lib/constants"
	"blacknc.top/ginapp/lib/framework/app"
	"blacknc.top/ginapp/lib/utils"
	"fmt"
	"strings"
	"time"
)
var (
	logitems = []string {
		"logid",
		"client_ip",
		"method",
		"api",
		"cookie",
		"user_agent",
		"cost",
		"errno",
		"errmsg",
		"bizerr",
	}
)

func Logger() app.WebHandlerFunc {

	return func(ctx *app.WebContext) {

		ctx.Set(constants.ReqBeginTime, time.Now())
		ctx.Set(constants.LogidKey, utils.GenLogid())

		ctx.Next()

		logFields := make(map[string]interface{})
		for _, logitem := range logitems {
			if handler, ok := handlers[logitem]; ok {
				logFields[logitem] = handler(ctx)
			}
		}
		ctx.WithFields(logFields).Info()
	}
}

var handlers = map[string]func(ctx *app.WebContext) string {
	"logid": func(ctx *app.WebContext) string {
		return ctx.MustGet(constants.LogidKey).(string)
	},
	"client_ip": func(ctx *app.WebContext) string {
		return ctx.ClientIP()
	},
	"method": func(ctx *app.WebContext) string {
		return ctx.Request.Method
	},
	"api": func(ctx *app.WebContext) string {
		return ctx.Request.RequestURI
	},
	"cookie": func(ctx *app.WebContext) string {
		var cookies []string
		for _, cookie := range ctx.Request.Cookies() {
			cookies = append(cookies, fmt.Sprintf("%s=%s", cookie.Name, cookie.Value))
		}
		return strings.Join(cookies, "; ")
	},
	"user_agent": func(ctx *app.WebContext) string {
		return ctx.Request.Header.Get("User-Agent")
	},
	"cost": func(ctx *app.WebContext) string {
		return time.Since(ctx.MustGet(constants.ReqBeginTime).(time.Time)).String()
	},
	"errno": func(ctx *app.WebContext) string {
		errno, ok := ctx.Get(constants.ReqErrnoKey)
		if ok {
			return errno.(string)
		} else {
			return ""
		}
	},
	"errmsg": func(ctx *app.WebContext) string {
		errmsg, ok := ctx.Get(constants.ReqErrmsgKey)
		if ok {
			return errmsg.(string)
		} else {
			return ""
		}
	},
	"bizerr": func(ctx *app.WebContext) string {
		bizerr, ok := ctx.Get(constants.ReqBizerrKey)
		if ok {
			return bizerr.(string)
		} else {
			return ""
		}
	},
}
