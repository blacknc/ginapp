package middlewares

import (
	"blacknc.top/ginapp/lib/constants"
	"blacknc.top/ginapp/lib/framework/app"
)

func ClientCancel(ctx *app.WebContext) {

	ctx.Next()

	select {
	case <-ctx.Request.Context().Done():
		{
			ctx.Set(constants.ReqErrmsgKey, "client canceled!!!")
		}
	default:
		{

		}
	}
}
