package middlewares

import (
	"blacknc.top/ginapp/lib/constants"
	"blacknc.top/ginapp/lib/errinfo"
	"blacknc.top/ginapp/lib/framework/app"
	"net/http"
	"time"
)

var (
	zero = struct{}{}
)

type Response struct {
	Errno  string      `json:"errno"`
	Errmsg string      `json:"errmsg"`
	Cost   string      `json:"cost"`
	Logid  string      `json:"logid"`
	Data   interface{} `json:"data"`
}

func DelResponse(ctx *app.WebContext) {

	ctx.Next()

	delResponse(ctx)
}

func delResponse(ctx *app.WebContext) {

	respData, ok := ctx.Get(constants.RespDataKey)
	if !ok {
		return
	}
	var errInfo *errinfo.RespErr
	if err, ok := respData.(error); ok {
		errInfo = errinfo.New(err)
		respData = zero
	} else {
		errInfo = errinfo.New(errinfo.ErrOK)
	}

	ctx.Set(constants.ReqErrnoKey, errInfo.GetErrno())
	ctx.Set(constants.ReqErrmsgKey, errInfo.GetErrmsg())
	ctx.Set(constants.ReqBizerrKey, errInfo.GetBizerr())

	resp := Response{
		Errno:  errInfo.GetErrno(),
		Errmsg: errInfo.GetErrmsg(),
		Cost:   time.Since(ctx.MustGet(constants.ReqBeginTime).(time.Time)).String(),
		Logid:  ctx.MustGet(constants.LogidKey).(string),
		Data:   respData,
	}
	if errInfo.ErrInfo == errinfo.ErrOK {
		ctx.JSON(http.StatusOK, resp)
	} else {
		ctx.AbortWithStatusJSON(http.StatusOK, resp)
	}
}
