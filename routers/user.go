package routers

import (
	"blacknc.top/ginapp/controllers/user"
	"blacknc.top/ginapp/lib/framework/app"
)

func userRouter(s *app.WebServer) {

	userGroup := s.Group("/user")
	userGroup.POST("/register", user.ControllerSingle.Register)
	userGroup.GET("/get", user.ControllerSingle.Get)
}