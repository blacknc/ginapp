package routers

import (
	"blacknc.top/ginapp/lib/framework/app"
	"blacknc.top/ginapp/middlewares"
)

func Init(s *app.WebServer) {

	s.Use(middlewares.ClientCancel)
	s.Use(middlewares.Logger())
	s.Use(middlewares.DelResponse)

	userRouter(s)
}
